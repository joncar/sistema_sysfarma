<style>
	.panel{
		border-radius:0px;
	}

	.error{
		border: 1px solid red !important;
	}
</style>
<div class="panel panel-default">
	<div class="panel-heading">
		<h1 class="panel-title">
			<b>Datos generales</b> | 
			ID caja: <span style="color:blue"><?= $this->user->caja ?></span> | 
			ID C.Diaria: <span style="color:blue"><?= $this->user->cajadiaria ?></span> | 
			Usuario: <span style="color:blue"><?= $this->user->nombre ?></span> | 
			Sucursal: <span style="color:blue"><?= $this->user->sucursalnombre ?></span> | 
			Fecha: <span style="color:blue"><?= date("d/m/Y H:i")  ?></span>
		</h1>
	</div>
	<div class="panel-body">
		<div class="row" style="position: relative;">
			<div style="width:100%; text-align: right; position:absolute; left:0; padding:0 30px">
				#FACTURA: <span id="nroFactura"></span>
			</div>
			<div class="col-xs-12 col-md-9">
				<div class="col-xs-12 col-md-4">
					Cliente <a href="#addCliente" data-toggle="modal" style="color:green"><i class="fa fa-plus"></i></a>: 
					<?php 
						$this->db->limit(1);
						echo form_dropdown_from_query('cliente','clientes','id','nro_documento nombres apellidos',1,'id="cliente"') 
					?>
				</div>
				<div class="col-xs-12 col-md-4">
					Transacción: 
					<?php 
						echo form_dropdown_from_query('transaccion',$this->db->get('tipotransaccion'),'id','denominacion',1,'id="transaccion"');
					?>
				</div>
				<div class="col-xs-12 col-md-4">
					Forma de Pago: 
					<?php 
						echo form_dropdown_from_query('forma_pago',$this->db->get('formapago'),'id','denominacion',1,'id="formapago"')
					?>
				</div>
				<div class="col-xs-12">
					Observaciones: 
					<input type="text" name="observaciones" id="observacion" class="form-control">				
				</div>		
			</div>
			<div class="col-xs-12 col-md-3" style="position: relative;">
				<span style="position: absolute;top: 26px;left: 30px;font-weight: bold;">Total Venta: </span>
				<input type="text" name="total_venta" class="form-control" readonly="" value="300.000" style="font-size:30px; font-weight:bold; text-align: right;height: 89px;vertical-align: baseline;margin-top: 19px;">
			</div>

		</div>

	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-md-9" style="padding-right: 39px;">
		<div class="panel panel-default" style="border:0">
			<div style="height:208px; overflow-y: auto;">
				<table class="table table-bordered" id="ventaDescr">
					<thead>
						<tr>
							<th>Código</th>
							<th>Nombre</th>
							<th>Cantidad</th>
							<th>Precio</th>
							<th>%Desc</th>
							<th>P.Desc</th>
							<th>Total</th>
						</tr>
					</thead>
					<tbody>

						<tr id="productoVacio">
							<td>
								<a href="javascript:void(0)" class="rem" style="display:none;color:red">
									<i class="fa fa-times"></i>
								</a> 
								<span>&nbsp;</span>
							</td>
							<td>&nbsp;</td>
							<td><input name="cantidad" class="cantidad" type="text" style="display:none; width:50px;text-align: right;padding: 0 6px;" value="0"></td>
							<td><input name="precio" class="precio" type="text" style="display:none; width:90px;text-align: right;padding: 0 6px;" value="0"></td>
							<td><input name="por_desc" class="por_desc" type="text" style="display:none; width:50px;text-align: right;padding: 0 6px;" value="0"></td>
							<td><input name="precio_descuento" class="precio_descuento" type="text" style="display:none; width:50px;text-align: right;padding: 0 6px;" value="0"></td>
							<td>&nbsp;</td>
						</tr>

						
					</tbody>
				</table>
			</div>

			<div class="panel-footer">
				<div class="row">
					<div class="col-xs-12 col-md-2" style="text-align: center;padding: 6px;">
						Cant: <span id="cantidadProductos">4</span>					
					</div>
					<div class="col-xs-12 col-md-10" style="position: relative;">
						<i class="fa fa-search" data-toggle="modal" data-target="#inventarioModal" style="color: #0fcb0f;font-weight: bold;position: absolute;top: 10px;left: 21px;cursor:pointer;"></i>
						<input id="codigoAdd" type="text" class="form-control" placeholder="Código de producto" style="padding-left: 25px;padding-right: 73px;">
						<button style="position: absolute;top: 1px;right: 16px;padding: 1px;" class="btn btn-primary">Insertar</button>

						<div id="searchProductShort" style="display:none; background: #fff;width: 86%;position: absolute;z-index: 1;border: 1px solid gray;"><ul><li style="list-style: none;"><a href="#">7842323343234 Producto de origen desconocido 1</a></li><li style="list-style: none;"><a href="#">7842323343234 Producto de origen desconocido 1</a></li><li style="list-style: none;"><a href="#">7842323343234 Producto de origen desconocido 1</a></li></ul></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-md-3" style="padding-left: 5px;">
		<div class="panel panel-default">
			<div class="panel-heading">Resumen de venta</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-xs-12 col-md-4">Pesos: </div>
					<div class="col-xs-8" style="margin-bottom:5px">						
						<input type="text" name="total_pesos" value="300.000" readonly="" style="width: 100%;">
					</div>
					<div class="col-xs-12 col-md-4">Reales: </div>
					<div class="col-xs-8" style="margin-bottom:5px">						
						<input type="text" name="total_reales" value="300.000" readonly="" style="width: 100%;">
					</div>
					<div class="col-xs-12 col-md-4">Dolares: </div>
					<div class="col-xs-8" style="margin-bottom:5px">						
						<input type="text" name="total_dolares" value="300.000" readonly="" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-heading">Atajos</div>
			<div class="panel-body">
				<span>(ALT+C) <small>Enfocar busqueda por código</small></span><br/>
				<span>(ALT+I) <small>Mostrar busqueda avanzada</small></span><br/>
				<span>(ALT+P) <small>Procesar venta</small></span><br/>
				<span>(ALT+N) <small>Nueva venta</small></span><br/>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-md-9">
		<div class="btn-group btn-group-justified" role="group" aria-label="...">
		  <div class="btn-group" role="group">
		    <button type="button" class="btn btn-default" onclick="nuevaVenta();">Nueva venta</button>
		  </div>
		  <div class="btn-group" role="group">
		    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#procesar">Procesar venta</button>
		  </div>
		  <div class="btn-group" role="group">
		    <button type="button" class="btn btn-default" disabled="true">Imprimir</button>
		  </div>
		</div>
	</div>
</div>

<div id="procesar" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog  modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Procesar venta</h4>
      </div>
      <div class="col-modal-body">
      	<div class="row">
      		<div class="col-xs-12" style="margin:0 30px">
      			<div class="radio">
				  <label>
				    <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
				    TICKET
				  </label>
				  <label style="margin-left:10px;">
				    <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
				    FACTURA LEGAL
				  </label>
				</div>
      		</div>
      	</div>
      	<div class="row">
      		<div class="col-xs-12 col-md-4">
      			<table class="table table-bordered">
      				<thead>
      					<tr>
      						<th colspan="2">Monto a pagar</th>
      					</tr>
      				</thead>
      				<tbody>
      					<tr>
      						<td>Pesos</td>
      						<td><input type="text" name="total_pesos" value="0" readonly="" style="width: 100%;"></td>
      					</tr>
      					<tr>
      						<td>Dolares</td>
      						<td><input type="text" name="total_dolares" value="0" readonly="" style="width: 100%;"></td>
      					</tr>
      					<tr>
      						<td>Reales</td>
      						<td><input type="text" name="total_reales" value="0" readonly="" style="width: 100%;"></td>
      					</tr>
      					<tr>
      						<td>Guaranies</td>
      						<td><input type="text" name="total_venta" value="0" readonly="" style="width: 100%;"></td>
      					</tr>
      				</tbody>
      			</table>   
      			<div id="pago_total" style="padding: 0px 30px;margin: 0 0 31px;">Gs.: <span class="totalGs" style="font-size: 30px;font-weight: bold;">0</span></div>   			
      		</div>
      		<div class="col-xs-12 col-md-4">
      			<table class="table table-bordered">
      				<thead>
      					<tr>
      						<th colspan="2">Pago</th>
      					</tr>
      				</thead>
      				<tbody>
      					<tr>
      						<td>Pesos</td>
      						<td><input type="text" name="pago_pesos" value="0" style="width: 100%;"></td>
      					</tr>
      					<tr>
      						<td>Dolares</td>
      						<td><input type="text" name="pago_dolares" value="0" style="width: 100%;"></td>
      					</tr>
      					<tr>
      						<td>Reales</td>
      						<td><input type="text" name="pago_reales" value="0" style="width: 100%;"></td>
      					</tr>
      					<tr>
      						<td>Guaranies</td>
      						<td><input type="text" name="pago_guaranies" value="0" style="width: 100%;"></td>
      					</tr>
      					<tr>
      						<td>Debito/Crédito</td>
      						<td><input type="text" name="total_debito" value="0" style="width: 100%;"></td>
      					</tr>
      				</tbody>
      			</table>
      		</div>
      		<div class="col-xs-12 col-md-4">
      			<table class="table table-bordered">
      				<thead>
      					<tr>
      						<th colspan="2">Vuelto</th>
      					</tr>
      				</thead>
      				<tbody>
      					<tr>
      						<td>Pesos</td>
      						<td><input type="text" name="vuelto_pesos" readonly="true" value="0" style="width: 100%;"></td>
      					</tr>
      					<tr>
      						<td>Dolares</td>
      						<td><input type="text" name="vuelto_dolares" readonly="true" value="0" style="width: 100%;"></td>
      					</tr>
      					<tr>
      						<td>Reales</td>
      						<td><input type="text" name="vuelto_reales" readonly="true" value="0" style="width: 100%;"></td>
      					</tr>
      					<tr>
      						<td>Guaranies</td>
      						<td><input type="text" name="vuelto" readonly="true" value="0" style="width: 100%;"></td>
      					</tr>
      				</tbody>
      			</table>
      		</div>
      	</div>
      	<div class="row">
      		<div class="col-xs-12">
      			<div class="respuestaVenta"></div>
      		</div>
      	</div>		
      </div>
      <div class="modal-footer">
      	<button type="button" class="btnNueva btn btn-info" onclick="nuevaVenta()" style="display: none">Nueva venta</button>
        <!--<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>-->
        <button type="button" onclick="sendVenta()" class="btn btn-primary">Guardar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div id="addCliente" class="modal fade" tabindex="-1" role="dialog">
  <form onsubmit="return insertar('maestras/clientes/insert',this,'.resultClienteAdd',function(data){setCliente(data)})">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">Agregar cliente</h4>
	      </div>
	      <div class="panel-body">
	  		
			  <div class="form-group">
			    <label for="exampleInputEmail1">Nombre</label>
			    <input type="text" class="form-control" name="nombres" placeholder="Nombre del cliente">
			  </div>
			  
			  <div class="form-group">
			    <label for="exampleInputEmail1">Apellido</label>
			    <input type="text" class="form-control" name="apellidos" placeholder="Apellido del cliente">
			  </div>

			  <div class="form-group">
			    <label for="exampleInputEmail1">#Documento</label>
			    <input type="text" class="form-control" name="nro_documento" placeholder="Documento del cliente">
			  </div>			  
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="submit" class="btn btn-primary">Guardar</button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
  </form>
</div><!-- /.modal -->

<?php $this->load->view('_inventario_modal',array(),FALSE,'movimientos'); ?>
<script>
<?php 
	if(!empty($edit)){
		$venta = $this->querys->getVenta($edit);
		if($venta){
			echo 'var editar = '.json_encode($venta).';';
		}else{
			echo 'var editar = undefined;';
		}
	}else{
		echo 'var editar = undefined;';
	}
?>
</script>
<script src="<?= base_url() ?>js/ventas.js"></script>
<script>	
	<?php
		$ajustes = $this->db->get('ajustes')->row();
	?>
	var tasa_dolar = <?= $ajustes->tasa_dolares ?>;
	var tasa_real = <?= $ajustes->tasa_reales ?>;
	var tasa_peso = <?= $ajustes->tasa_pesos ?>;

	var venta = new PuntoDeVenta();	
	if(editar!=undefined){
		console.log(editar);
		venta.setDatos(editar);
	}
	

	function initDatos(){
		venta.datos.sucursal = '<?= $this->user->sucursal ?>';
		venta.datos.caja = '<?= $this->user->caja ?>';
		venta.datos.cajadiaria = '<?= $this->user->cajadiaria ?>';
		venta.datos.fecha = '<?= date("Y-m-d H:i") ?>';	
		venta.datos.usuario = '<?= $this->user->id ?>';
	}
	initDatos();
	venta.initEvents();
	venta.updateFields();

	function imprimir(codigo){
        window.open('<?= base_url() ?>reportes/rep/verReportes/36/pdf/valor/'+codigo);
	}

	function imprimir2(codigo){
        window.open('<?= base_url('movimientos/ventas/getFactura/imprimir2/') ?>/'+codigo);
	}

	function imprimirTicket(codigo){		
        window.open('<?= base_url('movimientos/ventas/getFactura/imprimirticket/') ?>/'+codigo);
	}

	function selCod(codigo){		
		venta.addProduct(codigo);		
		$("#inventarioModal").modal('toggle');
	}

	function nuevaVenta(){		
		venta.initVenta();
		initDatos();
		$(".respuestaVenta").html('').removeClass('alert alert-info alert-danger alert-success');		
		if($("#procesar").css('display')=='block'){
			$("#procesar").modal('toggle');
		}
		$(".btnNueva").hide();
	}

	function sendVenta(){
		var datos =  JSON.parse(JSON.stringify(venta.datos));
		datos.productos = JSON.stringify(datos.productos);
		var accion = typeof(editar)=='undefined'?'insert':'update/'+editar.id;
		insertar('movimientos/ventas/ventas/'+accion,datos,'.respuestaVenta',function(data){						
			var id = data.insert_primary_key;						
			var enlace = '';
			$(".respuestaVenta").removeClass('alert-info');
			if($("input[name='optionsRadios']:checked").val()=='option1'){
				imprimirTicket(id);
				enlace = 'javascript:imprimirTicket('+id+')';
			}else{
				imprimir(id);
				enlace = 'javascript:imprimir('+id+')';
			}			
			$('.respuestaVenta').removeClass('alert alert-danger').addClass('alert alert-success').html(data.success_message+'<p>La factura se mostrará automáticamente, si no lo hace puede pulsar <a href="'+enlace+'">este enlace</a></p>');
			$(".btnNueva").show();
		});
	}

	function setCliente(data){
		if(data.success){
			$(".resultClienteAdd").html('<div class="alert alert-success">Cliente añadido con éxito</div>');
			$.post(URI+'maestras/clientes/json_list',{   
	            search_field:'id',     
	            search_text:data.insert_primary_key,
	            operator:'where'
	        },function(data){       
	            data = JSON.parse(data);   
	            venta.selectClient(data);
	        });
		}
	}

	$(document).on('shown.bs.modal',"#procesar",function(){
		$("#procesar input[name='pago_guaranies']").focus();
	});
</script>