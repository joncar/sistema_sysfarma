<?php

require_once APPPATH.'/controllers/Panel.php';    

class Ventas extends Panel {

    function __construct() {
        parent::__construct();
        if($this->router->fetch_method()!='selsucursal' && empty($this->user->sucursal)){
            header("Location:".base_url('panel/selsucursal'));
            die();
        }
        if($this->router->fetch_method()!='selcaja' && empty($this->user->caja)){
            header("Location:".base_url('panel/selcaja'));
            die();
        }
        if($this->router->fetch_method()!='selcajadiaria' && empty($this->user->cajadiaria)){
            header("Location:".base_url('panel/selcajadiaria'));
            die();
        }
    }
    
    public function ventas($x = '', $y = '') { 
        if($x=='nroFactura'){
            echo $this->querys->get_nro_factura();
            die();
        }       
        $crud = parent::crud_function($x, $y);
        $crud->set_theme('bootstrap2');
        $crud->unset_read()
             ->unset_delete();
        $crud->columns('transaccion', 'cliente', 'fecha', 'caja', 'nro_factura', 'total_venta', 'status');
        $crud->callback_column('nro_factura', function($val, $row) {
            return '<a href="javascript:showDetail('.$row->id.')">'.$val.'</a>';
        });
        $crud->callback_column('status', function($val, $row) {
            switch ($val) {
                case '0':return $this->user->admin==1?'Activa <a title="anular" href="javascript:anular(' . $row->id . ')"><i class="glyphicon glyphicon-remove"></i></a>':'Activa';
                    break;
                case '-1':return 'Anulada';
                    break;
                case '-2': return 'Anulada y procesada';
                    break;
            }
        });
        if($this->user->admin!=1){
            $crud->unset_edit();
        }
        $crud->set_relation('transaccion', 'tipotransaccion', 'denominacion')
             ->set_relation('cliente', 'clientes', '{nombres} {apellidos}')
             ->set_relation('sucursal', 'sucursales', 'denominacion');
        $crud->where('ventas.sucursal', $_SESSION['sucursal']);
        if (!empty($_SESSION['caja'])){
            $crud->where('ventas.caja', $_SESSION['caja']);
        }
        $crud->add_action('Creditos','',base_url('movimientos/creditos/creditos').'/');        
        $crud->order_by('id','DESC');
        
        ///Validations
        $crud->set_rules('total_venta','Total de venta','required|numeric|greater_than[0]');        
        $crud->set_rules('productos','Productos','required|callback_validar');                
        $crud->set_rules('nro_factura','Numero de factura','required|is_unique[ventas.nro_factura]');
        $crud->callback_after_insert(array($this,'addBody'));
        $crud->callback_after_update(array($this,'addBody'));
        $crud->unset_back_to_list();
        $output = $crud->render();   
        if($crud->getParameters()=='add' || $crud->getParameters()=='edit'){
            $edit = '';
            if($crud->getParameters()=='edit'){
                $edit = $y;                
            }
            $output->output = $this->load->view('ventas_new',array('output'=>$output->output,'edit'=>$edit),TRUE);
        }else{
            $output->output = $this->load->view('ventas_list',array('output'=>$output->output),TRUE);
        }
        $this->loadView($output);     
    }
    //Callback de function ventas a,b insert
    function addBody($post,$primary){
        $productos = json_decode($_POST['productos']);
        $pro = array();
        $this->db->delete('ventadetalle',array('venta'=>$primary));
        foreach($productos as $p){
            $pro[] = $p->nombre;
            $this->db->insert('ventadetalle',array(
                'venta'=>$primary,
                'producto'=>$p->codigo,
                'lote'=>'',
                'cantidad'=>$p->cantidad,
                'pordesc'=>$p->por_desc,
                'precioventa'=>$p->precio_venta,
                'precioventadesc'=>$p->precio_venta,
                'totalsindesc'=>0,
                'totalcondesc'=>$p->total,
                'iva'=>0,
                'inventariable'=>0
            ));
        }
        $this->db->update('ventas',array('productos'=>implode($pro)),array('id'=>$primary));
        //Actualizar correlativo
        $caja = $this->db->get_where('cajadiaria',array('caja'=>$_SESSION['caja'],'abierto'=>1));
        $correlativo = $caja->row()->correlativo+1;
        $this->db->update('cajadiaria',array('correlativo'=>$correlativo),array('caja'=>$_SESSION['caja'],'abierto'=>1));
    }

    function getFactura($x = '',$y = ''){
        if ($x == 'imprimir' || $x=='imprimir2' || $x == 'imprimirticket') {
            if (!empty($y) && is_numeric($y)) {
                $this->load->library('enletras');        
                $this->db->select('ventas.*, sucursales.denominacion, sucursales.telefono, sucursales.direccion, cajas.denominacion as caja, clientes.nombres as clientename, clientes.apellidos as clienteadress');
                $this->db->join('clientes', 'clientes.id = ventas.cliente');
                $this->db->join('cajas', 'cajas.id = ventas.caja');
                $this->db->join('sucursales', 'sucursales.id = ventas.sucursal');
                $venta = $this->db->get_where('ventas', array('ventas.id' => $y));
                if ($venta->num_rows() > 0) {
                    $detalles = $this->db->get_where('ventadetalle', array('venta' => $venta->row()->id));
                    if ($x == 'imprimir' || $x=="imprimir2" || $x=="imprimirticket") {
                        $view = $x=="imprimir"?"imprimir_factura":"imprimir_factura_linea";
                        $margen = $x=="imprimir"?array(5, 1, 5, 8):array(2, 5, 5, 8);
                        $papel = 'L';
                        if($x=="imprimir" || $x=="imprimir2"){
                            if(!isset($_GET['pdf']) || $_GET['pdf']!=0){
                                ob_clean();
                                $html2pdf = new HTML2PDF('L', array(240, 150), 'fr', false, 'ISO-8859-15', $margen);
                                $html2pdf->writeHTML(utf8_decode($this->load->view('reportes/'.$view, array('venta' => $venta->row(), 'detalles' => $detalles), TRUE)));
                                $html2pdf->Output('Factura Legal.pdf', 'D');
                            }else{
                                $this->load->view('reportes/'.$view, array('venta' => $venta->row(), 'detalles' => $detalles));
                            }
                        } else {
                            $output = $this->load->view('reportes/imprimirTicket', array('venta' => $venta->row(), 'detalles' => $detalles), TRUE);
                            echo $output;
                        }
                    } else {
                        echo "Factura no encontrada";
                    }
                } else {
                    echo 'Factura no encontrada';
                }
            }
        }
    }

    function validar(){
        $productos = $_POST['productos'];     
        $productos = count(json_decode($productos));
        if(empty($productos)){            
            $this->form_validation->set_message('validar','Debe incluir al menos un producto');        
            return false;
        }
        //Validar si el pago es completo
        $ajustes = $this->db->get('ajustes')->row();
        $pagoGs = 0;
        if(!empty($_POST['pago_guaranies'])){
            $pagoGs+= $_POST['pago_guaranies'];
        }
        if(!empty($_POST['pago_dolares'])){
            $pagoGs+= $_POST['pago_dolares']*$ajustes->tasa_dolares;
        }
        if(!empty($_POST['pago_reales'])){
            $pagoGs+= $_POST['pago_reales']*$ajustes->tasa_reales;
        }
        if(!empty($_POST['pago_pesos'])){
            $pagoGs+= $_POST['pago_pesos']*$ajustes->tasa_pesos;
        }
        if(!empty($_POST['total_debito'])){
            $pagoGs+= $_POST['total_debito'];
        }

        if($_POST['total_venta']>$pagoGs){
            $this->form_validation->set_message('validar','Debe pagar la totalidad de la factura');        
            return false;
        }        
        return true;
    }

    function ventas_detail($ventaid){
        $this->as['ventas_detail'] = 'ventadetalle';
        $crud = $this->crud_function('','');
        $crud->where('venta',$ventaid);
        $crud->columns('producto','cantidad','totalcondesc');
        $crud->callback_column('producto',function($val,$row){
            return $this->db->get_where('productos',array('codigo'=>$row->producto))->roW()->nombre_comercial;
        }); 
        $crud->unset_add()
             ->unset_edit()
             ->unset_delete()
             ->unset_export()
             ->unset_print()
             ->unset_read();
        $crud = $crud->render();
        echo $crud->output;

    }
    
    function next_nro_factura(){
        echo $this->querys->get_nro_factura();
    }
    /* Cruds */
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
