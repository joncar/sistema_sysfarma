<?php

require_once APPPATH.'/controllers/Panel.php';    

class Admin extends Panel {

    function __construct() {
        parent::__construct();
    }

    public function empleados($x = '', $y = ''){
        $crud = parent::crud_function($x, $y);            
        $crud->unset_delete();
        $output = $crud->render();
        $this->loadView($output);
    }
    
    public function vehiculos($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);    
        $crud->set_relation_dependency('modelos_id','marcas_id','marcas_id');   
        $crud->set_field_upload('foto_vehiculo','img/vehiculos');
        $crud->set_relation('clientes_id','clientes','{nro_documento} {nombres} {apellidos}');
        $crud->unset_delete();
        $output = $crud->render();
        $this->loadView($output);
    }    

    public function servicios($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);   
        $crud->set_lang_string('insert_success_message','Su factura se almaceno con éxito <script>document.location.href="'.base_url().'boxes/admin/servicios_detalles/{id}/add";</script>');;     
        $crud->field_type('facturado','hidden','0')
             ->field_type('anulado','hidden','0')
             ->field_type('nro_factura','hidden','')
             ->field_type('sucursales_id','hidden',$this->user->sucursal)
             ->field_type('cajadiaria_id','hidden',$this->user->cajadiaria);
        if($crud->getParameters()=='list'){
            $crud->field_type('facturado','true_false',array('0'=>'NO','1'=>'SI'));
        }
        if($crud->getParameters()=='list'){
            $crud->set_relation('clientes_id','clientes','{nro_documento}|{nombres}|{apellidos}');
        }else{
            $crud->set_relation('clientes_id','clientes','{nro_documento} {nombres} {apellidos}');
        }
        $crud->set_relation_dependency('vehiculos_id','clientes_id','clientes_id');
        $crud->columns('id','fecha','j3eb7f57f.nro_documento','j3eb7f57f.nombres','j3eb7f57f.apellidos','vehiculos_id','estado_servicio_id','facturado','total');    
        $crud->callback_column('total',function($val,$row){
            return $this->db->query('SELECT FORMAT(COALESCE(SUM(total)),0,"de_DE") as total from servicios_detalles where servicios_id = '.$row->id)->row()->total.' Gs.';
        })
        ->callback_column('j3eb7f57f.nro_documento',function($val,$row){
            return explode('|',$row->s3eb7f57f)[0];
        })
        ->callback_column('j3eb7f57f.nombres',function($val,$row){
            return explode('|',$row->s3eb7f57f)[1];
        })
        ->callback_column('j3eb7f57f.apellidos',function($val,$row){
            return explode('|',$row->s3eb7f57f)[2];
        })
        ->callback_field('empleados_id',function($val){
            $this->db->select('empleados.id, user.nombre, user.apellido');
            $this->db->join('user','user.id = empleados.user_id');
            return form_dropdown_from_query('empleados_id','empleados','id','nombre apellido',$val,'id="field-empleados_id"');
        })
        ->display_as('j3eb7f57f.nro_documento','#Documentos')
        ->display_as('j3eb7f57f.nombres','Nombres')
        ->display_as('j3eb7f57f.apellidos','Apellidos')
        ->display_as('vehiculos_id','Vehiculo')
        ->display_as('estados_servicio_id','Estado')
        ->display_as('id','#Servicio')
        ->order_by('id','DESC');
        $crud->add_action('Detalles','',base_url('boxes/admin/servicios_detalles/').'/');
        $crud->add_action('Facturar','',base_url('movimientos/ventas/ventas/add/').'/');
        $crud->unset_delete();
        $output = $crud->render();
        $this->loadView($output);
    }

    function servicios_detalles($x = '', $y = ''){
        if(is_numeric($x)){
            $servicio = $this->db->get_where('servicios',array('id'=>$x));
            if($servicio->num_rows()>0){
                $servicio = $servicio->row();
                $crud = parent::crud_function($x, $y);  
                if($servicio->facturado==1){
                    $crud->unset_delete()
                         ->unset_edit()
                         ->unset_add();                
                }          
                $crud->where('servicios_id',$x)
                     ->set_relation('productos_id','productos','nombre_comercial',array('anulado <'=>1))
                     ->field_type('servicios_id','hidden',$x)
                     ->unset_columns('servicios_id');
                $output = $crud->render();
                $header = new ajax_grocery_crud();
                $header->set_table('servicios')
                       ->set_theme('header_data')
                       ->set_subject('Servicio')
                       ->where('id',$x);
                $header->columns('fecha','clientes_id','vehiculos_id','estado_servicio_id','facturado','total');
                $header->callback_column('total',function($val,$row){
                    return $this->db->query('SELECT FORMAT(COALESCE(SUM(total)),0,"de_DE") as total from servicios_detalles where servicios_id = '.$row->id)->row()->total.' Gs.';
                });
                $header->set_url('boxes/admin/servicios/');
                $output->header = $header->render(1)->output;
                $output->servicio = $x;
                $output->output = '<div class="alert alert-warning">Si el servicio ya ha sido facturado no se permite realizar cambios.</div>'.$this->load->view('servicios',array('output'=>$output),TRUE);
                $this->loadView($output);
            }
        }
    }

}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
