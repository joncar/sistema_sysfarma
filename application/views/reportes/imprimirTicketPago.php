<html lang="es"><head>
        <title>Factura</title>
        <meta charset="utf-8">
      
</head>
<body style='font-size:11px; width:227px; margin:37px;'>
    <h3 align="center" style="font-size:20px; font-weight:bold; margin-bottom:5px"><?= $venta->denominacion ?></h3>
    <div align="center"><?= $venta->direccion ?></div>
    <div align="center">Telef. <?= $venta->telefono ?></div>
    <h3 align="center" style="border-bottom:1px solid black; border-top:1px solid black; font-size:20px; font-weight:bold; margin-bottom:5px;">Recibo de Pago</h3>
    <table style='width:100%; font-size:11px;'>
        <tr><td><b>Id. Pago: </b><?= $venta->id ?></td><td><b>Cajero/a</b> <?= $_SESSION['nombre'] ?></td></tr>
    </table>
<div><b>Fecha: </b><?= date("d/m/Y H:i:s",strtotime($venta->fecha)) ?></div>
<div><b>Caja: </b><?= $venta->caja ?></div>
<div><b>Cliente: </b><?= $venta->clientename.' '.$venta->clienteadress ?></div>
<div>
    <b>Saldo anterior: </b>
    <?php 
        $saldo = $this->db->query("
        SELECT
            clientes.nro_documento,
            clientes.nombres,
            clientes.apellidos,
            ifnull(clientes.celular1,'Falta_cargar') as celular,
            ifnull(max(date(ventas.fecha)),0) as ultima_compra,
            ifnull(SUM(ventadetalle.totalcondesc),0) as total_compra,
            ifnull(pagos.ultimo_pago,0) as ultimo_pago,
            ifnull(pagos.total_pagos,0) as total_pagos,
            ifnull(SUM(ventadetalle.totalcondesc),0)- ifnull(pagos.total_pagos,0) as saldo
            FROM ventas
            INNER JOIN clientes ON clientes.id = ventas.cliente
            INNER JOIN ventadetalle on ventadetalle.venta = ventas.id
            LEFT JOIN(
            SELECT 
            clientes.id as clienteid,
            max(date(pagocliente.fecha)) as ultimo_pago,
            sum(pagocliente.total_pagado) as total_pagos
            FROM pagocliente 
            INNER JOIN clientes on clientes.id = pagocliente.clientes_id
            WHERE pagocliente.anulado = 0 or pagocliente.anulado is null
            GROUP BY pagocliente.clientes_id
            ) as pagos on pagos.clienteid =ventas.cliente
            WHERE ventas.transaccion = 2 and ventas.status = 0 AND clientes.id = ".$venta->cliente."
            GROUP BY ventas.cliente"
        );
        $saldo = $saldo->num_rows()>0?$saldo->row()->saldo:0;
        $saldo+= $venta->total_pagado;
        echo number_format($saldo,0,',','.');
    ?>
</div>
<div><b>Monto Pagado: </b><?= number_format($venta->total_pagado,0,',','.'); ?></div>
<?php
    //Total venta
    $this->db->select('SUM(ventas.total_venta) as total_venta');
    $this->db->where('transaccion', '2');
    $this->db->where('ventas.status !=',-1);
    $total_venta = $this->db->get_where('ventas', array('ventas.cliente' => $venta->cliente));
    $totalVentas = $total_venta->row()->total_venta == null ? 0 : $total_venta->row()->total_venta;
    //Total pagos
    $this->db->select('SUM(pagocliente.total_pagado) as total_pagos');
    $total_pago = $this->db->get_where('pagocliente', array('pagocliente.clientes_id' => $venta->cliente));
    $totalPagos = $total_pago->num_rows() == 0 ? 0 : $total_pago->row()->total_pagos;
    $saldo = $totalVentas - $totalPagos;
?>
<div><b>Saldo actual: </b><?= number_format($saldo,0,',','.'); ?></div>

<p align='center' style="margin:10px; font-size:14px;"><i>Gracias por su pago</i></p>
</body>
<script>
    window.print();
</script>
</html>
