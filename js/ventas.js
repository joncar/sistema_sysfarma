var PuntoDeVenta = function(){
	
	this.productoHTML = $("#productoVacio").clone();
	this.productoHTML.removeAttr('id');
	this.productoHTML.find('a,input').show();
	this.productoshtml = $("#ventaDescr tbody");
	this.alt = false;
	this.initEvents = function(){
		var l = this;
		//Seleccionar cliente
		$(document).on('change','#cliente',function(){
			if($(this).val()!=''){
				l.datos.cliente = $(this).val();
				l.updateFields();
			}
		});
		//Buscar cliente
		var busc = false;
	    $(document).on('keyup',"#cliente_chzn input[type='text']",function(){      
	      var val = $(this).val();
	      setTimeout(function(){
	        if(val.length>3){
	          $.post(URI+'maestras/clientes/json_list',{   
	            search_field:'',     
	            search_text:val
	          },function(data){       
	            data = JSON.parse(data);   
	            l.selectClient(data,val);
	          });
	        }       
	      },600);
	    });

	    //Campo añadir codigo
	    $(document).on('change','#codigoAdd',function(){
	    	//l.addProduct($(this).val());
	    });
	    $(document).on('keyup','#codigoAdd',function(e){
	    	if(e.which==13){
	    		//$(this).trigger('change');
	    		l.addProduct($(this).val());
	    	}
	    });

	    $(document).on('keyup','input[name="pago_guaranies"],input[name="total_debito"]',function(e){	    	
	    	if(e.which==13){
	    		//$(this).trigger('change');
	    		sendVenta();
	    	}
	    });


	    /* Event precio, cantidad en productos */
	    $(document).on('change','.cantidad',function(){
	    	var cantidad = parseFloat($(this).val());
	    	var codigo = $(this).parents('tr').data('codigo');
	    	for(var i in l.datos.productos){
	    		if(l.datos.productos[i].codigo==codigo){
	    			l.datos.productos[i].cantidad = cantidad;	    			
	    		}
	    	}
	    	l.updateFields();
	    });

	    $(document).on('change','.precio',function(){
	    	var precio_venta = parseFloat($(this).val());
	    	var codigo = $(this).parents('tr').data('codigo');
	    	for(var i in l.datos.productos){
	    		if(l.datos.productos[i].codigo==codigo){
	    			l.datos.productos[i].precio_venta = precio_venta;	    			
	    		}
	    	}
	    	l.updateFields();
	    });

	    //Event calcular vuelto
	    $(document).on('change','input[name="efectivo"]',function(){
	    	l.datos.total_efectivo = parseFloat($(this).val());
	    	l.updateFields();
	    });

	    //Event Remove
	    $(document).on('click','.rem',function(){
	    	l.removeProduct($(this).parents('tr').data('codigo'));
	    });

	    //Event descuentos
	    $(document).on('change','.por_desc',function(){
	    	var tr = $(this).parents('tr').data('codigo');
	    	var descuento = parseFloat($(this).val());
	    	for(var i in l.datos.productos){
	    		l.datos.productos[i].por_desc = descuento;
	    		l.datos.productos[i].precio_descuento = (descuento*l.datos.productos[i].precio_venta)/100;
	    	}
	    	l.updateFields();
	    });
	    //Event descuentos
	    $(document).on('change','.precio_descuento',function(){
	    	var tr = $(this).parents('tr').data('codigo');
	    	var descuento = parseFloat($(this).val());
	    	for(var i in l.datos.productos){
	    		l.datos.productos[i].precio_descuento = descuento;
	    		l.datos.productos[i].por_desc = (descuento*100)/l.datos.productos[i].precio_venta;
	    	}
	    	l.updateFields();
	    });

	    //Event calcular vuelto
	    $(document).on('focus','input[name="pago_dolares"],input[name="pago_reales"],input[name="pago_pesos"],input[name="pago_guaranies"],input[name="total_debito"]',function(){
	    	var val = $(this).val();	    	
	    	val.replace(' ARP','');
	    	val.replace(' US$','');
	    	val.replace(' BRL','');
	    	val.replace(' PYG','');
	    	val = parseFloat(val);
	    	$(this).val(val);	    	
	    });

	    //Event calcular vuelto
	    $(document).on('change','input[name="pago_dolares"]',function(){
	    	l.datos.pago_dolares = !isNaN(parseFloat($(this).val()))?parseFloat($(this).val()):l.datos.pago_dolares;
	    	l.updateFields();
	    });

	    //Event calcular vuelto
	    $(document).on('change','input[name="pago_reales"]',function(){
	    	l.datos.pago_reales = !isNaN(parseFloat($(this).val()))?parseFloat($(this).val()):l.datos.pago_reales;
	    	l.updateFields();
	    });

	    //Event calcular vuelto
	    $(document).on('change','input[name="pago_pesos"]',function(){
	    	l.datos.pago_pesos = !isNaN(parseFloat($(this).val()))?parseFloat($(this).val()):l.datos.pago_pesos;
	    	l.updateFields();
	    });

	    //Event calcular vuelto
	    $(document).on('change','input[name="pago_guaranies"]',function(){
	    	l.datos.pago_guaranies = !isNaN(parseFloat($(this).val()))?parseFloat($(this).val()):l.datos.pago_guaranies;
	    	l.updateFields();
	    });

	    $(document).on('change','input[name="total_debito"]',function(){
	    	l.datos.total_debito = !isNaN(parseFloat($(this).val()))?parseFloat($(this).val()):l.datos.total_debito;
	    	l.updateFields();
	    });

	    //Otros campos
	    $(document).on('change','#transaccion,#formapago,#observacion',function(){l.updateFields();});

	    //Atajos
	    $(document).on('keydown',function(e){
	    	if(e.which==18){
	    		l.alt = true;
	    	}
	    });

	    //Atajos
	    $(document).on('keyup',function(e){
	    	if(e.which==18){
	    		l.alt = false;
	    	}else{
	    		switch(e.which){
	    			case 67: //[c] Focus en codigos
	    				$("#codigoAdd").focus();
	    			break;
	    			case 73: //[i] busqueda avanzada
	    				$("#inventarioModal").modal('toggle');
	    			break;
	    			case 80: //[p] busqueda avanzada
	    				$("#procesar").modal('toggle');
	    			break;
	    			case 78: //[n] Nueva venta
	    				nuevaVenta();
	    			break;
	    		}	    
	    		console.log(e.which);		
	    	}

	    });
	}

	this.selectClient = function(data,val){
		var opt = '';
        for(var i in data){
          opt+= '<option value="'+data[i].id+'">'+data[i].nro_documento+' '+data[i].nombres+' '+data[i].apellidos+'</option>';
        }
        if(data.length==0){
          opt+= '<option value="">Cliente no existe</option>';
        }
        $("#cliente").html(opt);
        $("#cliente").chosen().trigger('liszt:updated');
        $("#cliente_chzn input[type='text']").val(val);
    	$("#cliente").trigger('change');
    	this.updateFields();
	}

	this._addProduct = function(producto,cantidad){
		cantidad = cantidad!=undefined?cantidad:1;
		var pr = this.datos.productos;
		var enc = false;
		for(var i in pr){
			if(pr[i].codigo==producto.codigo){
				enc = true;
				this.datos.productos[i].cantidad+=cantidad;				
			}
		}
		if(!enc){
			this.datos.productos.push({
				codigo:producto.codigo,
				nombre:producto.nombre_comercial,
				cantidad:cantidad,
				por_desc:0,
				precio_venta:parseFloat(producto.precio_venta),
				precio_descuento:0,
				total: 0
			});
		}

		this.updateFields();
	}

	this.addProduct = function(codigo){
		//buscamos el codigo
		var l = this;				
		if(codigo!=''){
			codigo = codigo.split('*');
			if(codigo.length==2){
				cantidad = parseFloat(codigo[0]);
				codigo = codigo[1];
			}else{
				codigo = codigo[0];
				codigo = codigo.split('+');
				if(codigo.length==2){
					cantidad = parseFloat(codigo[0]);
					codigo = codigo[1];	
				}
				else{
					cantidad = 1;
				}
			}
			$.post(URI+'movimientos/productos/productos/json_list',{
				'search_field[]':'codigo',
				'search_text[]':codigo,
				'operator':'where',
				'cliente':this.datos.cliente
			},function(data){
				data = JSON.parse(data);
				if(data.length>0){
					$("#codigoAdd").val('');
					l._addProduct(data[0],cantidad);
					$("#codigoAdd").attr('placeholder','Código de producto');
					$("#codigoAdd").removeClass('error');
				}else{
					$("#codigoAdd").val('');
					$("#codigoAdd").attr('placeholder','Producto no encontrado');
					$("#codigoAdd").addClass('error');
				}
			});
		}
	}

	this.removeProduct = function(codigo){
		if(codigo!=''){
			var pr = this.datos.productos;
			var pr2 = [];
			for(var i in pr){
				if(pr[i].codigo!=codigo){
					pr2.push(pr[i]);
				}
			}
		}
		this.datos.productos = pr2;
		this.updateFields();
	}

	this.updateFields = function(){
		this.datos.cliente = $("#cliente").val();
		this.datos.transaccion = $("#transaccion").val();
		this.datos.forma_pago = $("#formapago").val();
		this.datos.observacion = $("#observacion").val();		
		
		//Calcular total
		var pr = this.datos.productos;
		var total = 0;
		for(var i in pr){
			this.datos.productos[i].total = (pr[i].cantidad*pr[i].precio_venta)-pr[i].precio_descuento;			
			total+= this.datos.productos[i].total;
		}
		this.datos.total_venta = total;
		//Calcular divisas
		this.datos.total_dolares = total/tasa_dolar;
		this.datos.total_reales = total/tasa_real;
		this.datos.total_pesos = total/tasa_peso;
		//Calcular acumulado
		this.datos.total_efectivo = 0;
		this.datos.total_efectivo+= this.datos.pago_guaranies;
		this.datos.total_efectivo+= tasa_dolar>0?this.datos.pago_dolares/tasa_dolar:0;
		this.datos.total_efectivo+= tasa_real>0?this.datos.pago_reales/tasa_real:0;
		this.datos.total_efectivo+= tasa_peso>0?this.datos.pago_pesos/tasa_peso:0;		
		//this.datos.vuelto = this.datos.vuelto<0?0:this.datos.vuelto;
		//Calcular acumulado
		this.datos.total_pagado = 0;		
		this.datos.total_pagado+= this.datos.pago_guaranies;
		this.datos.total_pagado+= tasa_dolar>0?this.datos.pago_dolares*tasa_dolar:0;
		this.datos.total_pagado+= tasa_real>0?this.datos.pago_reales*tasa_real:0;
		this.datos.total_pagado+= tasa_peso>0?this.datos.pago_pesos*tasa_peso:0;
		this.datos.total_pagado+= this.datos.total_debito>0?this.datos.total_debito:0;		
		//Calcular vuelto		
		this.datos.vuelto = this.datos.total_pagado>0?this.datos.total_pagado - this.datos.total_venta:0;
		this.datos.vuelto_dolares = this.datos.pago_dolares>0?this.datos.pago_dolares - this.datos.total_dolares:0;
		this.datos.vuelto_reales =  this.datos.pago_reales>0?this.datos.pago_reales - this.datos.total_reales:0;
		this.datos.vuelto_pesos =   this.datos.pago_pesos>0?this.datos.pago_pesos - this.datos.total_pesos:0;		
		this.datos.vuelto_dolares = this.datos.vuelto_dolares<0?0:this.datos.vuelto_dolares;
		this.datos.vuelto_reales =  this.datos.vuelto_reales<0?0:this.datos.vuelto_reales;
		this.datos.vuelto_pesos =   this.datos.vuelto_pesos<0?0:this.datos.vuelto_pesos;
		this.refresh();
	}

	this.refresh = function(){
		const gs = new Intl.NumberFormat('es-ES', {style: 'currency',currency: 'PYG',minimumFractionDigits: 0});
		const usd = new Intl.NumberFormat('es-ES', {style: 'currency',currency: 'USD',minimumFractionDigits: 2});
		const arg = new Intl.NumberFormat('es-ES', {style: 'currency',currency: 'ARP',minimumFractionDigits: 2});
		const br = new Intl.NumberFormat('es-ES', {style: 'currency',currency: 'BRL',minimumFractionDigits: 2});
		
		$("input[name='total_venta']").val(gs.format(this.datos.total_venta));
		$("input[name='total_pesos']").val(arg.format(this.datos.total_pesos));
		$("input[name='total_reales']").val(br.format(this.datos.total_reales));
		$("input[name='total_dolares']").val(usd.format(this.datos.total_dolares));
		$("input[name='pago_guaranies']").val(gs.format(this.datos.pago_guaranies));
		$("input[name='pago_pesos']").val(arg.format(this.datos.pago_pesos));
		$("input[name='pago_reales']").val(br.format(this.datos.pago_reales));
		$("input[name='pago_dolares']").val(usd.format(this.datos.pago_dolares));
		$("input[name='total_debito']").val(gs.format(this.datos.total_debito));
		$("input[name='efectivo']").val(this.datos.total_efectivo);
		$("input[name='vuelto']").val(gs.format(this.datos.vuelto));
		$("input[name='vuelto_pesos']").val(arg.format(this.datos.vuelto_pesos));
		$("input[name='vuelto_reales']").val(br.format(this.datos.vuelto_reales));
		$("input[name='vuelto_dolares']").val(usd.format(this.datos.vuelto_dolares));
		$("#total_total span").html(gs.format(this.datos.total_venta));
		$("#total_vuelto span").html(gs.format(this.datos.vuelto));
		$("#pago_total span").html(gs.format(this.datos.total_pagado));
		$("#cantidadProductos").html(this.datos.productos.length);		
		$("#procesar_total_efectivo").html(gs.format(this.datos.total_efectivo));
		$("#nroFactura").html(this.datos.nro_factura);
		//Draw products
		var pr = this.datos.productos;
		this.productoshtml.html('');		
		for(var i=pr.length-1;i>=0;i--){
			var newRow = this.productoHTML.clone();
			var td = newRow.find('td');
			newRow.attr('data-codigo',pr[i].codigo);
			$(td[0]).find('span').html(pr[i].codigo);
			$(td[1]).html(pr[i].nombre);
			$(td[2]).find('input').val(pr[i].cantidad);
			$(td[3]).find('input').val(pr[i].precio_venta.toFixed(0));
			$(td[4]).find('input').val(pr[i].por_desc.toFixed(2));
			$(td[5]).find('input').val(pr[i].precio_descuento.toFixed(0));
			$(td[6]).html(gs.format(pr[i].total));
			this.productoshtml.append(newRow);			
		}
		console.log(this.datos);
	}

	this.setDatos = function(datos){
		this.datos = datos;
		this.datos.total_venta = parseFloat(this.datos.total_venta);
		this.datos.total_pesos = parseFloat(this.datos.total_pesos);
		this.datos.total_reales = parseFloat(this.datos.total_reales);
		this.datos.total_dolares = parseFloat(this.datos.total_dolares);
		this.datos.vuelto = parseFloat(this.datos.vuelto);
		this.datos.total_efectivo = parseFloat(this.datos.total_efectivo);
		for(var i in this.datos.productos){
			this.datos.productos[i].cantidad = parseFloat(this.datos.productos[i].cantidad);
			this.datos.productos[i].precio_venta = parseFloat(this.datos.productos[i].precio_venta);
			this.datos.productos[i].por_desc = parseFloat(this.datos.productos[i].por_desc);
			this.datos.productos[i].precio_descuento = parseFloat(this.datos.productos[i].precio_descuento);
			this.datos.productos[i].total = parseFloat(this.datos.productos[i].total);
		}
	}

	this.initVenta = function(){
		this.datos = {			
			sucursal:'',
			caja:'',
			cajadiaria:'',
			fecha:'',
			nro_factura:'',
			usuario:'',
			cliente:0,
			transaccion:0,
			forma_pago:0,
			observacion:'',							
			total_pesos:0,
			total_descuentos:0,
			total_venta:0,
			total_dolares:0,
			total_reales:0,
			total_efectivo:0,
			pago_dolares:0,
			pago_reales:0,
			pago_pesos:0,
			pago_guaranies:0,
			total_debito:0,
			vuelto_dolares:0,
			vuelto_reales:0,
			vuelto_pesos:0,		
			vuelto:0,
			productos:[]
		};
		var l = this;
		$.get(URI+'movimientos/ventas/ventas/nroFactura',{},function(data){
			l.datos.nro_factura = data;
			l.refresh();
			$("#codigoAdd").focus();
		});
	}

	this.initVenta();
}

